# QU4RTET-UI Boilerplate

## Disclaimer

Working on plugin development can be hazardous to the servers that are loaded from the application. The hot reload (reloading code on the fly when it's been modified)
can force the UI to perform many requests on all of the servers as it reloads the navigation or different screens.
You should never work on a plugin while having production servers loaded into the User Interface. You're advised to follow the tutorials for QU4RTET
and launch a local development instance of the QU4RTET server (or Docker, VirtualBox, ...) when developing a new plugin.

## Application Architecture Overview

QU4RTET-UI is written in ES6/ES8. It uses the Electron ecosystem to package a React application into a standalone window. The architecture of the applications is therefore component-based. Both the Redux store and individual component states are used throughout the application (App-wide data tends to use Redux, while states used in the isolation of a component are using component state.)

Plugin development allows you to harness any of the components created in QU4RTET-UI as well as all packages installed by default in QU4RTET-UI. You can also install your own packages in node_modules, just as you would with any NodeJS package. Just like Atom (based on Electron as well), you can use any of the Core NodeJS modules (path, fs, ...), giving you a lot of freedom as to what your plugin can achieve.

It is highly recommended that you make yourself familiar with React and ES6+ prior to diving into this tutorial, since many of the basic concepts of React/Redux will not be covered in detailed here.

## Plugin Repository

In order for plugins to appear in the interface under the Plugins screen, they are listed in a json file available on Gitlab, under the quartet-ui-plugins project:
https://gitlab.com/serial-lab/quartet-ui-plugins

You may submit a pull request on the develop branch of this project when your plugin is ready. Once reviewed, it will be approved and available for all users to install subsequently.

## Boilerplate Code and Basics of Plugin Development

### Clone this project locally and get started.

Start by cloning this project in the directory of your choice:

`git clone https://gitlab.com/lduros/quartet-ui-plugins-boilerplate.git quartet-ui-plugin-[your-plugin-name]`

`cd quartet-ui-plugin-[your-plugin-name]`

Remove the .git folder.
`rm -rf .git`

Create a brand new git repo:

`git init`

`git add -A`

`git commit -a -m"first commit"`

Then run `yarn`

and `yarn start` to get started.

Let's now take a look at what is provided in this boilerplate,
and the values that will need to be changed in order for you to take advantage of it for your own plugin development.

### Adding the new plugin to the interface in dev/hot reload mode

In order to inject your new plugin and its components in the interface, you must use the Developer Plugin, available from the plugin list in QU4RTET UI.
Plugin development is available in both the production version of Qu4rtet UI (binary-installed version) or by starting QU4RTET UI from the gitlab repository.

A new hammer-shaped control button will appear after enabling the developer plugin.

![hammer](./documentation/images/1.png)

Clicking on it allows you to add development plugins to the interface and have them reload based-on file system changes:
Paste the full path of the plugin project directory into the input field, and press the play button.

![add plugin](./documentation/images/4.png)

The first change you will notice is that a new item is present in the navigation under each server nodes:

![navigation](./documentation/images/5.png)

The navigation item is titled "Boilerplate", and has a context menu on right click, and a child node when expanding the item.
It also leads to an example screen when clicking on "Boilerplate".

![route](./documentation/images/6.png)

Let's take a deeper dive into how the plugin just got enabled.
When taking a look at the package.json, we see that the script loaded upon enabling the plugin is lib/init.js. This is the transpiled version of src/init.js.
src/init.js is written in ES6/ES8, while lib/init.js is ES5 friendly.

src/init.js is where it all starts. QU4RTET-UI expects two functions when loading plugins, enablePlugin and disablePlugin.

Let's first take a look at enablePlugin:

    export const enablePlugin = () => {
      pluginRegistry.registerReducer(PLUGIN_NAME, "boilerplate", reducer);
      pluginRegistry.registerRoutes(PLUGIN_NAME, routes);
      pluginRegistry.setMessages(messages);
      pluginRegistry.registerCss(PLUGIN_NAME, styles);
      pluginRegistry.registerComponent(
        PLUGIN_NAME,
        NavRoot,
        actions.addToTreeServers
      );
    };

You'll notice all the lines of code under enablePlugin and disablePlugin make calls from a single object pluginRegistry.

This is a singleton object in QU4RTET-UI and a quick way to tell the running QU4RTET-UI instance what we want to add to the UI with our plugin.

`pluginRegistry.registerReducer(PLUGIN_NAME, "boilerplate", reducer);`

The first line registers a reducer, that is, dynamically injects a new reducer for Redux into the application.
We'll dive into this part, with Redux and Reducers, a little later, since this is the more advanced part of plugin development.

`pluginRegistry.registerRoutes(PLUGIN_NAME, routes);`

The second line in the function registers new routes into QU4RTET.
Think of routes as pages or screens that are made available, users will be able to navigate to these screens.

`pluginRegistry.setMessages(messages);`
the setMessage() call registers new messages. These are objects mapping keys to internationalized strings, or translations, for different languages.

`pluginRegistry.registerCss(PLUGIN_NAME, styles);`
The registerCss line injects new CSS into the main window of the QU4RTET-UI, allowing you to add new styles for your own components or to replace
the style of other components.

      pluginRegistry.registerComponent(
        PLUGIN_NAME,
        NavRoot,
        actions.addToTreeServers
      );

Finally, the registerComponent call adds an actual React Component to the interface.
In this particular example, we're adding the component NavRoot (A navigation Root Item) to the navigation tree under each server.

The way QU4RTET-UI is able to tell where to add the component is by the action we are passing the function, actions.addToTreeServers. This basically tells QU4RTET-UI, we want to inject this component `NavRoot` into the navigation tree of each server available.
Whenever a plugin is enabled, enablePlugin() gets triggered.

disablePlugin follows more or less the same logic, it tells the core UI to remove what we have added previously when enabling the plugin.

### How packages and files get imported.

If you've used ES6 and CommonJS before, you are probably familiar with the following way to load files and modules:

`import {myThing} from "somewhere";`

`const {myThing} = require("somewhere");`

`const myThing = require("somewhere").myThing;`

All these lines do the same thing, and you can load your own files and modules this way.
However, when we're loading components or modules that are part of Qu4rtet, we have to get them from the context of the parent application, rather than from the module.
This is why you'll see a slightly different way to load files and packages in Plugins.

in init.js of the boilerplate, you may notice we import some of the files the regular way, like this:

`import {NavRoot} from "./components/Nav/NavItems";`

This component NavRoot is located in the components folder under src/components/Nav/NavItems.js of the Boilerplate folder.

But when we're loading a file that lives in QU4RTET-UI core, we use the "require" function that's in the context of the core app.
To do so we use the qu4rtet global object, which offers many functions and variables that are useful for plugin development.

We mentioned pluginRegistry ealier as the singleton object meant to be used to tell QU4RTET-UI how our plugin is interacting with the core interface.

This is how we import the singleton from the core QU4RTET-UI into our plugin, because we do it from the context of the QU4RTET app, this is the same object as the one running in the main application:

`const {pluginRegistry} = qu4rtet.require("./plugins/pluginRegistration");`

The path here appears as relative, and it is relative to the core QU4RTET-UI source folder. You can take a look directly at the files you're importing using qu4rtet.require() by checking the files in the src/ directory of QU4RTET-UI. For instance, "./plugins/pluginRegistration" is located under `src/plugins/pluginRegistration`:
https://gitlab.com/serial-lab/quartet-ui/blob/master/src/plugins/pluginRegistration.js

Main third-party modules and libraries can also be imported using qu4rtet.require, if they are already available and installed in QU4RTET-UI.

For instance, you'll most likely need to import React in order to create your own components. While you could install a local version of React in your plugin and import it using any of the statements above, you should really use the copy of React that's already coming as part of QU4RTET-UI core and is available to your plugin at runtime. In order to do so, you must use the QU4RTET-UI require method:

`const React = qu4rtet.require("react");`

### Modifying a Screen

The Boilerplate plugin comes with a generic `Hello World` screen. Let's take a look at it to understand the composition of screens and starting changing some of the values dynamically.

#### Registered Routes

In init.js, we notice that a module is passed to the registeredRoutes method:
`pluginRegistry.registerRoutes(PLUGIN_NAME, routes);`

The routes object is imported from a few lines above the enablePlugin function definition:
`import routes from "./routes";`

At this point we do not know how many routes are registered with the plugin, but it can be quickly found out by taking a look at the routes.js file:

```
import {ExampleScreen} from "./components/Lists/ExampleScreen";
const React = qu4rtet.require("react");
const {Route} = qu4rtet.require("react-router");

export default (() => {
  return [
    <Route
      key="exampleScreen"
      path="/boilerplate/:serverID/example-screen"
      component={ExampleScreen}
    />
  ];
})();
```

A component named ExampleScreen is imported locally from the plugin folder.
The React object is imported from the main QU4RTET-UI, as well as a Route component from the react-router package.

The module exports an array (returned by a function immediately invoked) of Route components. There is only one Route defined in the file, this is our example screen:

```
<Route
  key="exampleScreen"
  path="/boilerplate/:serverID/example-screen"
  component={ExampleScreen}
/>
```

* The `key` prop is used by React, it should be unique for this Route across the entire application.
* The path is what will be used by your application to get to this screen. We notice that the path starts with the plugin name, and it is specific to a server, since the :serverID token is part of the path. In the application the :serverID token will be replaced with a real serverID. This will allow us to get specifics about the context of a given server.
* Finally, a component is tied to the Route, in this case, `ExampleScreen`.

#### The ExampleScreen Component

Let's take a look at the ExampleScreen component, defined in `src/Lists/ExampleScreen.js`. We already know that this is the screen that is reached when click on the `Boilerplate` navigation item in the navigation tree:
![route](./documentation/images/6.png)

While this screen doesn't do much except from displaying a title and message, it is a good illustration of how a screen can be created:

```
const React = qu4rtet.require("react");
const {Component} = React;
const {FormattedMessage} = qu4rtet.require("react-intl");
const {RightPanel} = qu4rtet.require("./components/layouts/Panels");
const {Card, Callout} = qu4rtet.require("@blueprintjs/core");

export class ExampleScreen extends Component {
  render() {
    return (
      <RightPanel
        title={<FormattedMessage id="plugins.boilerplate.exampleScreen" />}>
        <div className="large-cards-container">
          <Card>
            <h5>Hello World</h5>
            <Callout>This is an example of a screen.</Callout>
          </Card>
        </div>
      </RightPanel>
    );
  }
}
```

As noted previously in other modules, our first task in a screen is to import the React object, and the Component class, from the React package running in the core application.

We then import the FormattedMessage component:
`const {FormattedMessage} = qu4rtet.require("react-intl");`

This is used to translate messages in different languages, based on definitions in messages.js files throughout the application (we'll get into this a little later.)

We then import the RightPanel component, a core QU4RTET-UI component, used for screens. Finally, we also import visual elements from Blueprint JS, Card and Callout.

We then export a class inheriting from the React Component class. Because this is a very simple screen, the only method defined in the class in the render() method.
You can return custom components or basic HTML components using the render method.

Whenever instantiating the RightPanel component, we define a title and pass it a FormattedMessage element. The message ID of the FormattedMessage is plugins.boilerplate.exampleScreen. This is how React-Intl will find the corresponding string in the right language. You can find the exampleString message defined in src/messages.js of the boilerplate plugin:
`exampleScreen: "Example Screen"`

Changing its value will change the title appearing in the left panel of the screen.
For instance, switching to `exampleScreen: "Developer Info"` will change the value displayed on the left:
![msg](./documentation/images/7.png)
![msg](./documentation/images/8.png)

Let's now replace the strings in the right panel with actual meaningful text.
We're changing the key "exampleScreen" in both the US English and French message objects to "developerInfo":
![msg](./documentation/images/9.png)

We then notice a small problem, the component is still pointing to `plugins.boilerplate.exampleScreen` as its message:

![msg](./documentation/images/10.png)

We change the id in the FormattedMessage component to fix this:
![msg](./documentation/images/11.png)

The title now gets back to normal:

![msg](./documentation/images/12.png)

And the French version is also displayed when switching languages:

![msg](./documentation/images/13.png)

Let's now take a look at the Card element in the RightPanel:

We notice that the text is actually literal strings there. `Hello World` isn't the most exciting title, so let's change the message to be the same as the title on the left:
![msg](./documentation/images/14.png)

The title in the Card element is now the same as the one in the left panel:
![msg](./documentation/images/15.png)

#### Accessing Dynamic Data

Let's now dive a little bit into what we'd like to display in our newly titled Developer Info screen.

By opening the Developer Tools (from the menu `View` followed by `Toggle Developer Tools`)

As we saw earlier, we used the pluginRegistry singleton to tell QU4RTET-UI what we want to add to the interface, but pluginRegistry is also useful to get information about the application and the server.

In Dev mode, by typing `pluginRegistry` in the console part of the Developer Tools, you'll get an idea of the methods and objects in the object:
![pluginRegistry](./documentation/images/16.png)

pluginRegistry can be used to retrieve information about servers or perform actions on a given server. Using the serverID, it's also possible to get a hold of a Server object.
Running `Object.keys(pluginRegistry._servers)` from the console will yield an array with all the serverIDs available in the loaded app.
In this instance, we only have a single server:
`bc5e4ff6-518e-4fb4-86fa-e3d088c32ce9`

By running the following in the console, we get a hold of the Server instance:
`pluginRegistry.getServer("bc5e4ff6-518e-4fb4-86fa-e3d088c32ce9")`

The server object has a variety of methods and functions, all defined in src/lib/servers.js of the core QU4RTET-UI project.
It contains all the information necessary to connect to the server, as well as methods to POST, DELETE, and GET from the server.

For our Developer Info page, let's try to list all of the operations that can be performed on the server and provide them to the interface user as information.

To take get a sneak peek of all the operations available, let's take a look again at the server object from the developer tools console.

```
// As a shortcut here from the console, you can assign the following to the server variable: pluginRegistry._servers[" followed by TAB to get the
// serverID without having to type or copy it
var server = pluginRegistry.getServer("bc5e4ff6-518e-4fb4-86fa-e3d088c32ce9");
var apis = null;
server.getClient().then(client => {
  apis = client.apis;
});
```

And now let's take a look at what our apis look like, it's an object with nested object for each module enabled in the QU4RTET backend server. To get a list of the modules we can do this:

```
Object.keys(apis).map(key => { return key; });
> (13) ["", "capture", "epcis", "group", "manifest", "masterdata", "output", "permission", "read-only-groups", "read-only-users", "rest-auth", "serialbox", "user"]
```

This is however not enough information to display for our users, we really want to get down to each operation (creating, deleting, updating, ...) that can be performed on the server.
So instead we'll do this, creating an object, and remapping the apis object of nested objects to return the name of the operationId for each module:

```
var operationsByModule = {};
Object.keys(apis).forEach(moduleName => {
  operationsByModule[moduleName] = Object.keys(apis[moduleName]).map(op => {
    return op;
  });
});
```

![operationIds](./documentation/images/17.png)

We are now getting a nice object with module names as keys and arrays of operationId names. This is the data we want to display in our Developer Info page. All we'll need to do now is pass this into a component that will make it easy for the user to navigate the list.

In order to do this, a collapsible nested HTML tree seems particularly adequate, and it so happens that the Blueprintjs library provides such a Tree component to handle hierarchical data. Please note, the actual navigation tree in QU4RTET is not an instance of the Blueprintjs Tree component, due to the dynamic nature of the navigation tree. However, the navigation tree is still very close in nature to the Blueprint tree, and this example will help us get a basic understanding of how a nested tree structure can be created in a component-based manner.

Let's get back to our ExampleScreen.js file, and add the Tree and ITreeNode components to it.
All we have to do is change this line:

`const {Card, Callout} = qu4rtet.require("@blueprintjs/core");`

to this one to import extra components from blueprint:

`const {Card, Callout, Tree, ITreeNode} = qu4rtet.require("@blueprintjs/core");`

We should now have the elements we need, let's create an APITree component so as not to bloat the rest of the ExampleScreen component:

```
class APITree extends Component {
  constructor(props) {
    super(props);
    this.state = {nodes: []};
  }
}
```

We start by initializing the state of the component with an empty nodes array.
This is what will contain our API names.
Let's now add a render method:

```
class APITree extends Component {
  constructor(props) {
    super(props);
    this.state = {nodes: []};
  }
  render() {
    return (<Tree
      contents={this.state.nodes}
      onNodeClick={this.handleNodeClick}
      onNodeCollapse={this.handleNodeCollapse}
      onNodeExpand={this.handleNodeExpand}
    />);
  }
}
```

this.state.nodes will represents how the tree structures itself, but since this is an interactive tree (allowing to expand and collapse), we need to define the methods that will be triggered for these events; this is more or less taken from an example in the Blueprint docs:

```
class APITree extends Component {
  constructor(props) {
    super(props);
    this.state = {nodes: []};
  }
  render() {
    return (
      <Tree
        contents={this.state.nodes}
        onNodeClick={this.handleNodeClick}
        onNodeCollapse={this.handleNodeCollapse}
        onNodeExpand={this.handleNodeExpand}
      />
    );
  }
  handleNodeClick = (nodeData, _nodePath, evt) => {
    const originallySelected = nodeData.isSelected;
    nodeData.isSelected =
      originallySelected == null ? true : !originallySelected;
    this.setState(this.state);
  };
  handleNodeCollapse = nodeData => {
    nodeData.isSelected = false;
    this.setState(this.state);
  };
  handleNodeExpand = nodeData => {
    nodeData.isExpanded = true;
    this.setState(this.state);
  };
}
```

Let's now take a look at the data structure that the Tree component expects, it's an array of objects, which can contain nested objects under the childNodes key.
Before we try to hook up the dynamic data into the tree, let's try to populate our tree with dummy data and see if the tree is displayed properly. Here we're adding the data after componentDidMount, one of the many stages in React's component life cycle:

```
class APITree extends Component {
  constructor(props) {
    super(props);
    this.state = {nodes: []};
  }
  componentDidMount() {
    let nodes = [
      {id: 1, icon: "folder-close", label: "Item 1"},
      {
        id: 2,
        icon: "folder-close",
        label: "Item 2",
        childNodes: [{id: 3, icon: "document", label: "Nested Item 1"}]
      }
    ];
    this.setState({nodes: nodes});
  }
  render() {
    return (
      <Tree
        contents={this.state.nodes}
        onNodeClick={this.handleNodeClick}
        onNodeCollapse={this.handleNodeCollapse}
        onNodeExpand={this.handleNodeExpand}
      />
    );
  }
  handleNodeClick = (nodeData, _nodePath, evt) => {
    const originallySelected = nodeData.isSelected;
    nodeData.isSelected =
      originallySelected == null ? true : !originallySelected;
    this.setState(this.state);
  };
  handleNodeCollapse = nodeData => {
    nodeData.isSelected = false;
    this.setState(this.state);
  };
  handleNodeExpand = nodeData => {
    nodeData.isExpanded = true;
    this.setState(this.state);
  };
}
```

When the page refreshes, nothing appears yet, that's because we aren't rendering our APITree element into the ExampleScreen component. Let's place the Tree under the Callout element:

```
export class ExampleScreen extends Component {
  render() {
    return (
      <RightPanel
        title={<FormattedMessage id="plugins.boilerplate.developerInfo" />}>
        <div className="large-cards-container">
          <Card>
            <h5>
              <FormattedMessage id="plugins.boilerplate.developerInfo" />
            </h5>
            <Callout>This is an example of a screen.</Callout>
            <APITree />
          </Card>
        </div>
      </RightPanel>
    );
  }
}
```

Great, our tree with dummy data now appears in the Card element:
![Tree1](./documentation/images/18.png)

We know the object with arrays we got back from our data manipulation from the console for our API, so now let's take this bit of code into our file and adapt it further so that it get displayed on the page:
