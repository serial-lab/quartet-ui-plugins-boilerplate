// Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
const React = qu4rtet.require("react");
const {Component} = React;
const {FormattedMessage} = qu4rtet.require("react-intl");
const {RightPanel} = qu4rtet.require("./components/layouts/Panels");
const {Card, Callout, Tree, ITreeNode} = qu4rtet.require("@blueprintjs/core");

class APITree extends Component {
  constructor(props) {
    super(props);
    this.state = {nodes: []};
  }
  componentDidMount() {
    let nodes = [
      {id: 1, icon: "folder-close", label: "Item 1"},
      {
        id: 2,
        icon: "folder-close",
        label: "Item 2",
        childNodes: [{id: 3, icon: "document", label: "Nested Item 1"}]
      }
    ];
    this.setState({nodes: nodes});
  }
  render() {
    return (
      <Tree contents={this.state.nodes} onNodeClick={this.handleNodeClick} />
    );
  }
  handleNodeClick = (nodeData, _nodePath, evt) => {
    const originallySelected = nodeData.isSelected;
    nodeData.isSelected =
      originallySelected == null ? true : !originallySelected;
    this.setState(this.state);
  };
  handleNodeCollapse = nodeData => {
    nodeData.isSelected = false;
    this.setState(this.state);
  };
  handleNodeExpand = nodeData => {
    nodeData.isExpanded = true;
    this.setState(this.state);
  };
}

export class ExampleScreen extends Component {
  render() {
    return (
      <RightPanel
        title={<FormattedMessage id="plugins.boilerplate.developerInfo" />}>
        <div className="large-cards-container">
          <Card>
            <h5>
              <FormattedMessage id="plugins.boilerplate.developerInfo" />
            </h5>
            <Callout>This is an example of a screen.</Callout>
            <APITree />
          </Card>
        </div>
      </RightPanel>
    );
  }
}
